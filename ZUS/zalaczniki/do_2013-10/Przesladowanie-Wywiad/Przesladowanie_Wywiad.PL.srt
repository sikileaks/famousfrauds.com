1 
00:00:00,000 --> 00:00:03,583 
>> MAREK PODLECKI: Dzień dobry, rozmawiam z Michałem Siemaszko. 

2 
00:00:04,341 --> 00:00:12,934 
Jeśli Państwo nie pamiętają kim jest Michał, radzę obejrzeć stare materiały, które nagrywaliśmy w 2009 roku z Piotrem Beinem z Kanady. 

3 
00:00:13,324 --> 00:00:22,028 
Michał, poznaliśmy się przy okazji akcji anty-szczepionkowej w 2009 roku, to Ty byłeś tym który prowadził grupę na Facebooku... 

4 
00:00:22,148 --> 00:00:27,874 
... to Ty byłeś tą osobą, która próbowała przynajmniej założyć stowarzyszenie "Cywilizacja Życia". 

5 
00:00:27,994 --> 00:00:35,640 
>> MICHAŁ SIEMASZKO: Współpracowaliśmy w 2009 roku przy okazji akcji mającej na celu powstrzymanie przymusowych szczepień. 

6 
00:00:35,677 --> 00:00:40,704 
Ja prowadziłem wtedy serwis m.in. na Facebooku, też jako stronę internetową, "Świńska Sprawa". 

7 
00:00:41,282 --> 00:00:44,709 
>> MAREK: To Ty byłeś m.in. organizatorem tego dużego spotkania w Teatrze Groteska. 

8 
00:00:44,775 --> 00:00:48,963 
>> MICHAŁ: Tak, współorganizowaliśmy spotkanie z Panią Jane Burgermeister w Teatrze Groteska. 

9 
00:00:49,180 --> 00:00:53,208 
... z Panem Piotrem Beinem też organizowaliśmy spotkanie w Krakowie. 

10 
00:00:53,544 --> 00:00:59,622 
Pomagałem Panu Piotrowi, Panu Markowi. Prowadziliśmy akcje ulotkowe, plakatowe, kampanię internetową. 

11 
00:00:59,658 --> 00:01:06,120 
Również chciałem zarejestrować stowarzyszenie, w celu ruszenia tych spraw i żeby to nie była jednorazowa akcja... 

12 
00:01:06,240 --> 00:01:22,103 
... ale żeby można było działać w tematach różnych, bo nie chodziło bynajmniej i nadal nie chodzi tylko o szczepienia, natomiast o różne inne sposoby systematycznej masowej depopulacji ludzi, o których sobie ludzie nie zdają sprawy. 

13 
00:01:22,448 --> 00:01:26,917 
Moje stowarzyszenie "Cywilizacja Życia" zostało od środka rozwalone... 

14 
00:01:27,728 --> 00:01:30,803 
.. po prostu nawet nie doszło do zarejestrowania przez to, że miałem podstawionych ludzi. 

15 
00:01:31,090 --> 00:01:36,858 
Natomiast to wszystko się zaczęło troszkę wcześniej, po śmierci mojej Córki w 2007 roku. 

16 
00:01:36,978 --> 00:01:38,658 
>> MAREK: No właśnie nie wiedziałem czy chcesz o tym mówić... 

17 
00:01:38,778 --> 00:01:44,873 
...wiedziałem o tej śmierci. Nie znałem tych szczegółów, które teraz znam, które są dla mnie na prawdę szokujące. 

18 
00:01:44,993 --> 00:01:48,669 
Proszę Państwa, to co mówimy to jest tylko część tego co możemy puścić. 

19 
00:01:48,942 --> 00:01:52,596 
Wiele rzeczy nie możemy puścić, z wielu względów - Państwo by po prostu nie uwierzyli w te rzeczy... 

20 
00:01:52,793 --> 00:01:59,656 
... Córka Michała i Marii, w ósmym miesiącu ciąży, została zamordowana. 

21 
00:02:01,946 --> 00:02:06,455 
>> MICHAŁ: Ponieważ było to moje pierwsze dziecko, przez wszystkie pierwsze miesiące ciąży jeździliśmy na USG... 

22 
00:02:06,493 --> 00:02:10,720 
.. były robione badania systematyczne, raz na dwa tygodnie, przynajmniej raz na miesiąc. 

23 
00:02:11,777 --> 00:02:18,767 
W szóstym miesiącu ciąży wyszły jakieś sprawy związane z serduszkiem. Pojechaliśmy specjalnie do specjalisty w Warszawie. 

24 
00:02:19,153 --> 00:02:23,634 
Zostało zweryfikowane i nie był specjalnie zaniepokojony sytuacją. 

25 
00:02:25,347 --> 00:02:32,007 
Matka Toli została wzięta do szpitala porodowego, gdzie spędziła miesiąc czasu. 

26 
00:02:32,266 --> 00:02:37,536 
Ja, będąc przy porodzie, też uczestniczyłem w tym wszystkim od samego początku. 

27 
00:02:37,783 --> 00:02:44,230 
Tola została zabrana od razu po porodzie do Szpitala Dziecięcego w Prokocimiu i zmarła po 2 tygodniach. 

28 
00:02:45,149 --> 00:02:48,379 
Miała kilka operacji, m.in. operacje jelita, operacje serca. 

29 
00:02:49,168 --> 00:02:56,797 
Niestety dopiero po 3 latach, po tym jak się rozstałem z Panią Marią, zaczęły wychodzić różne szczegóły dotyczące genezy tego. 

30 
00:02:56,917 --> 00:03:02,751 
Okazało się, że dziecko zostało specjalnie zabite jeszcze w trakcie ciąży, poprzez podanie środków farmakologicznych. 

31 
00:03:02,871 --> 00:03:05,778 
Nie była to aborcja, ponieważ aborcja jest dopuszczana tylko do 3 miesiąca ciąży. 

32 
00:03:05,813 --> 00:03:10,248 
>> MAREK: (Szpital Dziecięcy w Prokocimiu) to taki szpital amerykański, z którego wiele dzieci wyszło z autyzmem. Ja znam osobiście dzieci z autyzmem. 

33 
00:03:10,273 --> 00:03:13,341 
>> MICHAŁ: Nie wiem czy szpital w tym akurat uczestniczył. Nie wiem, nie mogę tego potwierdzić. 

34 
00:03:13,782 --> 00:03:23,921 
Natomiast to miało miejsce bodajże w 6 czy 7 miesiącu ciąży, kiedy zostały jej podane albo sama wzięła środki, które spowodowały wady z którymi dziecko się urodziło. 

35 
00:03:24,203 --> 00:03:27,458 
Natomiast sam szpital - też nie mogę powiedzieć za wiele dobrych rzeczy o tym szpitalu. 

36 
00:03:27,740 --> 00:03:34,226 
W każdym razie, wszystkie dokumenty związane z moim dzieckiem zostały zabrane w momencie kiedy się rozstałem z Panią Marią. 

37 
00:03:34,627 --> 00:03:37,607 
Pozostał mi tylko akt zgonu i akt urodzenia mojego dziecka. 

38 
00:03:38,977 --> 00:03:48,121 
Co więcej, okazało się, że osoby mi najbliższe też były zaangażowane m.in. w to co miało miejsce z Tolą, a również w kradzież kilkuset tysięcy złotych. 

39 
00:03:48,150 --> 00:03:51,550 
>> MAREK: No właśnie, ja Cię pamiętam bardzo majętnego. Mieliście bardzo fajne rowery... 

40 
00:03:51,869 --> 00:03:56,925 
... przyjeżdżałeś z najnowszym modele laptopa, Macintosh'a o ile się nie mylę. 

41 
00:03:57,045 --> 00:04:01,026 
Nie miałeś problemów z finansami. Lepiej wyglądałeś. 

42 
00:04:01,146 --> 00:04:06,163 
A teraz, przyznam Państwu szczerze, Michał się do mnie zgłosił, bo nie miał za co zjeść. 

43 
00:04:06,283 --> 00:04:13,912 
>> MICHAŁ: Zgłosiłem się do Pana Marka, ponieważ po 3 latach od 2010 roku, kiedy robiłem to co mogłem... 

44 
00:04:14,032 --> 00:04:20,963 
... systematycznie będąc banowany i, tak na prawdę, prześladowany. 

45 
00:04:21,272 --> 00:04:31,320 
Może wróćmy do 2010 roku. Miałem powiedziane przez przynajmniej 2-3 osoby, z którymi po rozstaniu się z Panią Marią chciałem te wszystkie sprawy poukładać... 

46 
00:04:31,440 --> 00:04:36,520 
... sensownie, że przez akcję szczepionkową, żebym przestał zajmować się tymi sprawami, ponieważ będę miał problemy. 

47 
00:04:36,949 --> 00:04:44,799 
Ja nie zamierzam, nie zamierzałem i nigdy nie przestanę się zajmować tymi sprawami, do póki te sprawy istnieją i do póki takie rzeczy mają miejsce. 

48 
00:04:44,859 --> 00:04:54,254 
Także, z mojej strony nic się nie zmieni. Do póki żyję, nie przestanę się zajmować sprawami, które dotykają mnie i potencjalnie dzieci, które chciałbym mieć. 

49 
00:04:54,348 --> 00:05:00,173 
>> MAREK: Właśnie, czy to dziecko Twoje, zabite, nie było jedną z przyczyn tego, że zająłeś się tą akcją anty-szczepionkową. 

50 
00:05:00,217 --> 00:05:10,666 
>> MICHAŁ: Oczywiście, że tak, ale akcja anty-szczepionkowa to była tylko jedna z rzeczy. Ja chciałem kontynuować tą działalność pod egidą stowarzyszenia, które zostało rozwalone od środka. 

51 
00:05:10,911 --> 00:05:19,051 
Ponieważ tych tematów jest bardzo dużo. To chodzi m.in. o żywność GMO przecież, o geoinżynierię, chodzi o przymusowe szczepienia. 

52 
00:05:19,171 --> 00:05:22,542 
>> MAREK: Muszę przyznać, że Michał jest bardzo obeznany w tych tematach. To będą kolejne rozmowy. 

53 
00:05:22,662 --> 00:05:26,463 
Na razie się skupmy na tym Michał co się z Tobą dzieję, bo potrzebujesz pomocy. 

54 
00:05:26,547 --> 00:05:30,053 
W grupie Twojej na Facebook'u było około tysiąc osób, o ile nie więcej, prawda? 

55 
00:05:30,173 --> 00:05:35,021 
>> MICHAŁ: W grupie, tak, było ponad tysiąc osób, natomiast strona była odwiedzana przez kilkadziesiąt tysięcy osób. 

56 
00:05:35,050 --> 00:05:39,866 
Natomiast to, co miało miejsce od tego czasu, to może wspomnę: 

57 
00:05:40,361 --> 00:05:46,696 
Przynajmniej przy 5 czy 6 okazjach, gdy wynajmowałem mieszkanie czy pokój w różnych miejscach w Polsce... 

58 
00:05:46,816 --> 00:05:53,072 
... przynajmniej przy kilku razach wchodzono podczas mojej nieobecności czy podczas snu do miejsca gdzie mieszkałem. 

59 
00:05:53,346 --> 00:05:54,809 
>> MAREK: .. na co masz dowody. 

60 
00:05:54,929 --> 00:06:00,929 
>> MICHAŁ: To znaczy mam zdjęcia niektórych tych sytuacji. Poza tym, wiem jaki efekt był na moim organizmie. 

61 
00:06:00,982 --> 00:06:05,849 
Zostałem przynajmniej dwukrotnie zaszczepiony, bez mojej zgody oczywiście. 

62 
00:06:06,132 --> 00:06:09,594 
I w tym wszystkim może Marku najważniejsze jest to, żeby mówić o tym. 

63 
00:06:09,651 --> 00:06:14,006 
Ja to co mogę robić, to robię, natomiast chodzi o to, żeby mówić o tym... 

64 
00:06:14,041 --> 00:06:19,925 
... bo osoby, które robią te rzeczy, polegają na tym, że nie jest się widocznym, prawda, że oni mogą zrobić wszystko... 

65 
00:06:20,045 --> 00:06:24,441 
... natomiast Ty nie masz jak, w jakikolwiek sposób się zrewanżować... 

66 
00:06:24,561 --> 00:06:28,937 
... ponieważ, no po prostu mnie nie było stać przez ostatnie 3 czy 4 lata na to, żeby wynająć adwokata... 

67 
00:06:28,984 --> 00:06:34,830 
... czy żeby odpowiednie ekspertyzy sporządzić, żeby zacząć egzekwować to, co jest moje. 

68 
00:06:34,950 --> 00:06:41,323 
Także najważniejsze żeby mówić o tym, a w między czasie oczywiście, owszem, chciałbym jak najszybciej stanąć finansowo "na nogi". 

69 
00:06:41,698 --> 00:06:49,893 
Mimo tego, że robię to, co mogę, miałem przynajmniej cztery czy pięć projektów - to są kolejne straty na kilkadziesiąt tysięcy złotych - ustawianych... 

70 
00:06:49,936 --> 00:06:56,600 
... gdzie, mimo wykonywania moich obowiązków, komunikacji z moim przełożonym, byłem nagle zwalniany z projektu. 

71 
00:06:56,831 --> 00:07:01,212 
No, nie wiem, te osoby myślą, że jak ja zacznę pracować już w jakiejś korporacji... 

72 
00:07:01,332 --> 00:07:08,594 
... nagle te tematy, jak właśnie przymusowe szczepienia, geoinżynieria, czy żywność modyfikowana genetycznie, czy wiele innych tematów... 

73 
00:07:08,981 --> 00:07:13,471 
... ja stwierdzę, że to jest w ogóle w jakikolwiek sposób dopuszczalne, czy to co miało miejsce wcześniej jest dopuszczalne. 

74 
00:07:13,591 --> 00:07:15,255 
To nie jest i nie będzie dopuszczalne. 

75 
00:07:15,281 --> 00:07:19,224 
Także, jeżeli ja z kimś współpracuję, to wykonując moje obowiązki... 

76 
00:07:19,344 --> 00:07:29,939 
... moje zobowiązania wobec tej osoby są tylko takie, jak na umowie. Ja nie mam obowiązku w jakikolwiek sposób ideologicznie, czy w jakikolwiek inny sposób, podlegać tej osobie. Także, tak to wygląda. 

77 
00:07:30,422 --> 00:07:32,635 
Natomiast najważniejsze, żeby o tym mówić, prawda. 

78 
00:07:33,495 --> 00:07:38,916 
>> MAREK: Widzę, że przygotowałeś trochę materiałów tutaj na swoim laptopie, który musi być zasilany z sieci, bo bateria nie działa. 

79 
00:07:39,172 --> 00:07:43,631 
>> MICHAŁ: No, laptop jest w takim stanie w jakim jest. Natomiast pieniądze to rzecz nabyta, prawda. 

80 
00:07:44,907 --> 00:07:51,533 
Ja od dziewiętnastego roku życia byłem na swoim, więc akurat od tych trzech lat to jest pierwszy taki okres, 

81 
00:07:51,653 --> 00:07:56,005 
kiedy - tylko i wyłącznie przez to, że jest to celowe działanie - 

82 
00:07:56,452 --> 00:08:00,366 
to jest stalking, to jest gang stalking, to jest systematyczne. 

83 
00:08:00,486 --> 00:08:07,069 
>> MAREK: Chwilę Ci przerwę, przepraszam. Dla Państwa, którzy nie wierzą, że coś takiego się może wydarzyć, ja przytoczę przykład Jane Burgermeister. 

84 
00:08:07,189 --> 00:08:09,261 
Kobieta w tej chwili się ukrywa. 

85 
00:08:09,392 --> 00:08:12,580 
Przeniosła się do Berlina. Nie wiemy w jakim kraju jest w tym momencie. 

86 
00:08:13,024 --> 00:08:16,364 
Pytałem się osoby jasnowidzącej. Powiedziała, że "Gdzieś w Europie". 

87 
00:08:16,852 --> 00:08:22,951 
Jane Burgermeister była zaatakowana, i to wiele razy, środkami o których Michał mówi. 

88 
00:08:23,113 --> 00:08:28,085 
Jest to osoba bardzo mocna. Michał jest również osobą bardzo mocną. Podejrzewam, że dlatego te osoby żyją. 

89 
00:08:28,513 --> 00:08:35,200 
Czy Michał swoją sytuację porównujesz do tego co się dzieje z Jane Burgermeister, o której zaczęło być cicho? To jest niebezpieczna sytuacja. 

90 
00:08:35,320 --> 00:08:42,948 
>> MICHAŁ: Może wspomnij o tym Marek, że Jane została też zwalniana z pracy nagle. Jej ojciec został zabity. 

91 
00:08:43,439 --> 00:08:47,311 
Miała różne problemy. Jej najbliżsi znajomi nagle się odwrócili od niej. 

92 
00:08:47,431 --> 00:08:56,725 
Po prostu, że tak powiem, celowe działania mające na celu uciszenie, wyciszenie danej osoby i zdyskredytowanie. 

93 
00:08:56,845 --> 00:09:00,557 
>> MAREK: I zdyskredytowanie jej przy okazji, bo mówiono nawet, że jej nie ma, że nie istnieje. 

94 
00:09:00,755 --> 00:09:07,396 
>> MICHAŁ: Dokładnie. I co więcej, ja miałem wielokrotnie groźby na zasadzie: "We will erase you" { "Usuniemy Cię" }, 

95 
00:09:07,613 --> 00:09:12,087 
"You are being hunted" { "Polujemy na Ciebie" } oraz inne groźby karalne. 

96 
00:09:12,623 --> 00:09:16,000 
Tzw. "Hunger games", czyli co zostało potwierdzone wielokrotnie. 

97 
00:09:16,661 --> 00:09:22,855 
>> MAREK: Ja przerwę znowu. Michał, Ty się posługujesz bardzo dobrze językiem angielskim. Wchodzisz w angielski, ale dlaczego. 

98 
00:09:22,975 --> 00:09:29,960 
Dlatego, że ta cała sprawa, to nie dotyczy tylko Polski, bo siły z którymi gramy to są te siły illuminackie. 

99 
00:09:30,459 --> 00:09:33,066 
To są międzynarodowe lichwiarskie powiązania. 

100 
00:09:33,186 --> 00:09:34,108 
>> MICHAŁ: Oczywiście. 

101 
00:09:34,179 --> 00:09:38,537 
>> MAREK: I dlatego Michał jest taką osobą niebezpieczną, ponieważ doskonale znasz angielski. 

102 
00:09:38,657 --> 00:09:43,329 
>> MICHAŁ: No, znam angielski. Akurat od dwudziestu lat się uczę i używam języka angielskiego. 

103 
00:09:43,714 --> 00:09:50,549 
To jest też m.in. jedna z rzeczy, którą mogę jeszcze zaoferować - tłumaczenia m.in. - ale o tym porozmawiamy później. 

104 
00:09:50,580 --> 00:09:56,197 
Poza tym, do materiału będą też dołączone informacje z odnośnikami, gdzie moje ogłoszenia są wystawione, 

105 
00:09:56,224 --> 00:10:04,481 
Ja nie proszę o pieniądze bynajmniej, ja po prostu proszę o zlecenia i to wszystko, bo ja nie jestem osobą niepełnosprawną, żebym nie mógł pracować. 

106 
00:10:04,601 --> 00:10:12,121 
Chodzi o to tylko, że dotychczas z Markiem się nie skontaktowałem, bo nie wiedziałem, że jest to na tylu frontach, tak perfidnie zorganizowane, 

107 
00:10:12,162 --> 00:10:18,548 
Gdzie, ja mimo moich największych starań, najlepszych chęci, jestem cały czas "udupiany", za przeproszeniem i blokowany. 

108 
00:10:18,807 --> 00:10:25,000 
Więc, z końcem sierpnia (2013 roku), kilka tygodni temu się skontaktowałem z Markiem, bo po prostu już nie wiedziałem co mogę zrobić. 

109 
00:10:25,394 --> 00:10:29,997 
Kiedy kolejny projekt okazywało się, że nagle nie dostaje zlecenia, gdzie przygotowywałem oferty. 

110 
00:10:30,381 --> 00:10:35,171 
Robiłem to co mogłem. Znowu np. nie mogę się dodzwonić. Moje maile nie dochodzą. 

111 
00:10:35,185 --> 00:10:41,364 
>> MAREK: Nie tylko nie dochodzą, ale są wykorzystywane, bo ja wiem, że ktoś się pod Ciebie podszywa, żeby Ci koło d*** zrobić, po prostu. 

112 
00:10:41,484 --> 00:10:42,595 
>> MICHAŁ: To też, to też. 

113 
00:10:42,767 --> 00:10:46,317 
Także, może kontynuujmy. "Hunger games" czyli gry jedzeniem. 

114 
00:10:46,333 --> 00:10:52,162 
Chodzi o to, że ponieważ te działania były systematyczne - stalking, blokowanie możliwości zarabiania, zwalnianie nagle, 

115 
00:10:52,282 --> 00:10:56,256 
okradanie, wiele wiele innych. Możliwości normalnej komunikacji (blokowane).

116 
00:10:56,304 --> 00:11:02,919 
Gry, które mają miejsce od kilku lat, są intencjonalne i systematyczne. Rezultatem czego wielokrotnie, bardzo często nawet, nie miałem co jeść. 

117 
00:11:03,039 --> 00:11:07,231 
Żywiłem się ledwo co chlebem z masłem i wodą. A nawet z tym czasem był problem. 

118 
00:11:07,453 --> 00:11:16,381 
Teraz było lato, także miałem epizody, gdzie dzięki temu, że znalazłem pole z jeżynami, mogłem przez dwa tygodnie żywić się jeżynami. 

119 
00:11:18,409 --> 00:11:19,897 
>> MAREK: No bo wegetarianinem jesteś. 

120 
00:11:19,950 --> 00:11:24,417 
>> MICHAŁ: Jestem wegetarianinem, tak, ale przez zimę, jesień, no to już jest ciężko takie miejsca znaleźć, prawda? 

121 
00:11:25,907 --> 00:11:31,390 
Generalnie, jest to nagminnie łamanie prawa człowieka. Czułem się jak niewolnik. Nadal czuję się jak niewolnik. 

122 
00:11:31,405 --> 00:11:41,315 
Jakbym co najmniej należał, moje życie należało do kogoś. Już nie wiem czy do "Państwa" - mówię o rządzie - czy ktokolwiek jest w to zamieszany. 

123 
00:11:41,809 --> 00:11:47,541 
Moje życie należy do mnie. Ja mam prawo decydować o moim życiu. Ja mam prawo decydować z kim i gdzie chce mieszkać. 

124 
00:11:47,921 --> 00:11:54,890 
Jeżeli wkładam w coś moją pracę, to chcę widzieć rezultaty tej pracy, a nie żeby moja praca była cały czas przejmowana przez kogoś innego. 

125 
00:11:55,181 --> 00:12:00,969 
Żeby moje pieniądze były mi kradzione. Żeby moje zdrowie było niszczone. Gdzie ja nie mam możliwości w jakikolwiek sposób "stanąć na nogi", 

126 
00:12:01,261 --> 00:12:06,614 
wynająć adwokata czy ekspertyzy wykonać, ponieważ cały czas jestem finansowo, że tak powiem, w dołku. 

127 
00:12:07,225 --> 00:12:09,618 
Cały czas jestem w tak zwanym "trybie przetrwalnikowym". 

128 
00:12:09,738 --> 00:12:11,700 
Także, może jeszcze przejdźmy do następnej sprawy. 

129 
00:12:11,820 --> 00:12:16,048 
Dwa razy byłem aresztowany i to był taki sprawdzian, troszkę. 

130 
00:12:16,603 --> 00:12:21,376 
Zdarzyło mi się parę razy, że kradłem jedzenie ze sklepu, ponieważ potrzebowałem zjeść. 

131 
00:12:21,397 --> 00:12:27,297 
Nie było to lato, a ja stwierdziłem, że jeżeli takie rzeczy mają miejsce, dlaczego ja nie mam mieć co jeść. 

132 
00:12:27,601 --> 00:12:29,959 
Także, parę razy zdarzyło mi się ukraść owszem ze sklepu jedzenie. 

133 
00:12:29,973 --> 00:12:37,369 
>> MAREK: Proszę Państwa, informatyk, znakomicie znający angielski, który mógłby zarabiać 60 tysięcy funtów rocznie. 

134 
00:12:37,489 --> 00:12:41,224 
>> MICHAŁ: Ja zarabiałem w 2010 roku 10 tysięcy złotych, na fakturę. Takie faktury wystawiałem. 

135 
00:12:41,258 --> 00:12:46,630 
Otrzymuje propozycje z Angli na 60 tysięcy funtów (rocznie), natomiast nie stać mnie na to, żeby np. pojechać tam. 

136 
00:12:46,715 --> 00:12:48,402 
>> MAREK: Na relokacje. 
>> MICHAŁ: Na relokacje nie stać mnie. 

137 
00:12:48,424 --> 00:12:51,053 
>> MICHAŁ: Nie stać mnie na to, żeby wynająć mieszkanie czy mieć na jedzenie. 

138 
00:12:51,063 --> 00:12:53,251 
>> MAREK: Poza tym, nie masz pewności, gdzie trafisz też. 

139 
00:12:53,298 --> 00:12:57,806 
>> MICHAŁ: Poza tym, nie mam pewność, czy to nie będzie kolejny ustawiany projekt, ponieważ miało miejsce to już przynajmniej pięć czy sześć razy. 

140 
00:12:58,073 --> 00:13:04,675 
Nie mam pewności, kto mnie tam zaprasza, więc bez zabezpieczenia finansowego, nie zrobię tak, żeby na przykład pożyczyć ewentualnie te pieniądze. 

141 
00:13:04,847 --> 00:13:09,307 
>> MAREK: Ja uważam, że bardzo dobrze teraz robisz, że próbujesz się zabezpieczyć, bo wiemy z kim mamy do czynienia. 
>> MICHAŁ: Muszę się zabezpieczyć. 

142 
00:13:09,382 --> 00:13:12,307 
>> MAREK: Zostałeś zaszczepiony. Powiedz o tym, bo to jest coś niesamowitego. 

143 
00:13:12,323 --> 00:13:19,214 
>> MICHAŁ: Tak, przynajmniej dwa razy podczas snu, wchodzono mi do mieszkania, gdzie mieszkam - czy wynajmowałem mieszkanie czy pokój - i zostałem zaszczepiony. 

144 
00:13:19,510 --> 00:13:23,926 
Objawiało się to w ten sposób, że przez dwa - trzy tygodnie oblewały mnie poty, 

145 
00:13:24,046 --> 00:13:29,837 
Czułem problemy z oddychaniem. Miałem później problemy z myśleniem. 

146 
00:13:29,877 --> 00:13:32,682 
W szczepionkach są, m.in. metale ciężkie. 

147 
00:13:33,286 --> 00:13:39,326 
>> MAREK: Oni mają różne sposoby. Ja pisałem o tym, jak mnie atakowano za pomocą, podejrzewałem, fali infradźwiękowych. 

148 
00:13:39,811 --> 00:13:43,740 
Zastałem ekipę, która wyglądała na wyglądała na ekipę Mossadu, u mnie w mieszkaniu. 

149 
00:13:44,111 --> 00:13:47,024 
I, rzekomo sprawdzali napięcie, czy jest zgodne z normą europejską... 

150 
00:13:47,241 --> 00:13:49,684 
... co elektrownia w ogóle wyśmiała - niczego takiego nie było. 

151 
00:13:49,901 --> 00:13:52,221 
Ja Ci Michał wierze - mnie próbowano zlikwidować. 

152 
00:13:52,265 --> 00:13:57,000 
>> MICHAŁ: Wiesz co Marek, Ty mi wierzysz, ja Tobie wierze, ale to są rzeczy, które ja potwierdziłem u przynajmniej 5 - 6 niezależnych osób. 

153 
00:13:57,058 --> 00:14:02,031 
Ja od 2010 roku byłem na spotkaniach z wieloma osobami, gdzie przychodziłem z listą i rozmawiałem z tymi osobami... 

154 
00:14:02,151 --> 00:14:06,905 
... i te osoby po kolei mi potwierdzały te rzeczy. To nie są rzeczy, które czy Marek czy ja sobie uroiliśmy. 

155 
00:14:07,230 --> 00:14:11,027 
Marek tak samo ma kontakt z innymi ludźmi, z którymi rozmawia i te osoby mu potwierdzają to... 

156 
00:14:11,060 --> 00:14:16,360 
... także to nie jest jedna - dwie osoby, tylko to są osoby, które niezależnie potwierdzają te same informacje. 

157 
00:14:16,480 --> 00:14:21,856 
Ja miałem już 3 czy 4 lata temu mówione, przez niektóre osoby, z którymi mam cały czas kontakt... 

158 
00:14:21,976 --> 00:14:27,370 
... rzeczy do których musiałem dojść przez ostatnie 3 lata, ponieważ były tak niewiarygodne dla mnie... 

159 
00:14:27,490 --> 00:14:34,432 
... dopiero przez doświadczenie, przez wydobycie materiału z podświadomości, dopiero zaczęło do mnie to dochodzić, ponieważ dotyczyło to mojej najbliższej rodziny. 

160 
00:14:34,552 --> 00:14:40,922 
Także, może jeszcze wracając do aresztowań. Za pierwszym razem, kiedy wspomniałem o jednym z miejsc, w którym mieszkałem... 

161 
00:14:41,195 --> 00:14:50,517 
... gdzie moje zdrowie było dosyć mocno nadszarpnięte - to jest, na ulicy Kostrzewskiego w Krakowie. 

162 
00:14:50,851 --> 00:14:57,202 
Gdy wspomniałem o ulicy Kostrzewskiego, zostałem szybko wypuszczony, mimo tego, że grożono mi wcześniej nocką w areszcie. 

163 
00:14:57,322 --> 00:15:04,735 
Za drugim razem natomiast, kiedy wspomniałem o innej osobie, gdzie miałem ustawiany projekt, w Brzozowie, w województwie podkarpackim... 

164 
00:15:05,053 --> 00:15:12,662 
... ta osoba nagle, mimo wykonywania moich obowiązków, zaczęła mi grozić pobiciem i kazała mi się wynieść z biura... 

165 
00:15:12,782 --> 00:15:16,624 
... ja oczywiście poszedłem na Policję i zgłosiłem zawiadomienie o popełnieniu przestępstwa. 

166 
00:15:16,744 --> 00:15:22,248 
Kiedy wspomniałem o tym, Policjanci nagle mnie wypuścili, dając mi najniższy możliwy mandat. 

167 
00:15:22,368 --> 00:15:26,893 
>> MAREK: Dwadzieścia złotych. 
>> MICHAŁ: Dwadzieścia złotych, tak. Mimo tego, że też mi grozili, że będę w areszcie siedział. 

168 
00:15:27,013 --> 00:15:35,837 
>> MICHAŁ: Także, to mówi, że to nie chyba przestępczość zorganizowana, tylko albo przestępczość zorganizowana współpracująca z organami ścigania ma/miała w tym udział... 

169 
00:15:35,957 --> 00:15:43,632 
... a wiadomo jak illuminaci działają, prawda - Policja tak na prawdę wykonuje rozkazy... 

170 
00:15:43,690 --> 00:15:50,429 
... są zobligowani do wykonywania rozkazów.  Na szczęście nie wszyscy Policjanci są skorumpowani, niektórzy potrafią myśleć. 

171 
00:15:50,476 --> 00:15:52,346 
>> MAREK: Większość nie jest. 

172 
00:15:52,466 --> 00:15:55,021 
>> MICHAŁ: Niestety, część nie ma pojęcia w ogóle o tym co robi... 

173 
00:15:55,303 --> 00:16:01,248 
... i że wykonują rozkazy ludzi, którzy mimo tego, że są na wysokich stanowiskach, są kryminalistami i psychopatami. 

174 
00:16:03,219 --> 00:16:11,163 
>> MAREK: O czym wielokrotnie pisałem. 
>> MICHAŁ: O czym wielokrotnie Marek pisał i co zostało potwierdzone przez ich działania przez ostatnie kilka lat. Także to tak w skrócie mniej więcej wygląda. 

175 
00:16:11,283 --> 00:16:16,725 
>> MICHAŁ: Marek, pytaj. 
>> MAREK: Teraz może przejdźmy do tego kluczowego momentu - jak można Tobie pomóc. 

176 
00:16:17,802 --> 00:16:21,349 
>> MICHAŁ: Tzn, tak ja mówię, ja nie jestem osobą niepełnosprawną. 

177 
00:16:21,375 --> 00:16:30,153 
Ja potrzebuję, po prostu, jak najszybciej zebrać odpowiednie środki, żeby np. zrobić sobie detoks z nagromadzonych toksyn, wyniku szczepień. 

178 
00:16:31,226 --> 00:16:33,023 
>> MAREK: Chcesz wyczyścić organizm. 
>> MICHAŁ: Chcę wyczyścić organizm. 

179 
00:16:33,064 --> 00:16:36,333 
>> MICHAŁ: Między innymi przez to, co miało miejsce, mam cukrzycę. 

180 
00:16:36,453 --> 00:16:41,045 
Szczepienia powodują cukrzycę typu I. Ja właśnie mam przez to, co miało miejsce. 
>> MAREK: A nie miałeś. 

181 
00:16:41,088 --> 00:16:47,076 
>> MICHAŁ: Nie miałem bynajmniej wcześniej. Przez ingerencję w mój organizm mam cukrzycę stopnia I. 

182 
00:16:47,515 --> 00:16:53,386 
Cztery miesiące temu miałem zlecone ponad 15 badań krwi, niestety nie było mnie stać na to, żeby wykonać te badania. I mam m.in. też kandydozę. 

183 
00:16:53,770 --> 00:17:01,469 
No i oczywiście wyjść z "trybu przetrwalnikowego". Mi ledwo co starcza na czynsz i na jedzenie, więc ja potrzebuję po prostu mieć zlecenia stałe. 

184 
00:17:01,589 --> 00:17:03,341 
>> MAREK: Żebyś przeżył. 
>> MICHAŁ: Żebym przeżył, tak. 

185 
00:17:03,362 --> 00:17:09,872 
>> MAREK: Ja tylko przerwę na moment Michałowi. Kilka dni temu byłem u osoby jasnowidzącej - więcej, u egzorcystki. 

186 
00:17:09,992 --> 00:17:18,987 
Ona nie potrzebuje nawet widzieć zdjęcia danej osoby, żeby do tej osoby dotarła potrzebuje wyobrażenia. I ona sobie wyobraziła Michała. 

187 
00:17:19,107 --> 00:17:26,349 
I się zapytałem tej osoby, tej egzorcystki, na ten temat. Powiedziała wyraźnie: Pan Michał wymaga oczyszczenia. 

188 
00:17:26,796 --> 00:17:31,742 
Nie wiem, co jeszcze w nim jest - czy jest zaczipowany, czy coś innego - ale to by było potwierdzenie tego, o czym mówisz. 

189 
00:17:31,862 --> 00:17:37,987 
>> MICHAŁ: Tzn. ja się oczyszczam - to zaraz może do tego przejdziemy. Ja oczyszczam  swoje ciało energetyczne regularnie, praktycznie codziennie. 

190 
00:17:38,063 --> 00:17:40,227 
>> MAREK: Ale nie jesteś pewny, czy przypadkiem Cię nie zaczipowali. 

191 
00:17:41,382 --> 00:17:50,465 
>> MICHAŁ: Jeżeli chodzi o to, to jak najbardziej. Także to bardziej w tym kierunku. Ja bynajmniej z satanizmem ani z takimi energiami nie mam do czynienia, wręcz przeciwnie. 

192 
00:17:50,812 --> 00:17:56,768 
Natomiast, jak można mi pomóc - do materiału na pewno będzie dołączona notka moja. Ja oferuję m.in. usługi... 

193 
00:17:56,804 --> 00:18:00,154 
>> MAREK: No właśnie, powiedz. Może zmieńmy język - 
Let's switch to English, OK? 

194 
00:18:00,274 --> 00:18:03,245 
You can present what you can say in English, OK? 
{ "Możesz pokazać jak mówisz po angielsku, OK?" } 

195 
00:18:03,365 --> 00:18:07,662 
Let's talk about your translations - what you can do in English. 
{ "Porozmawiajmy o tłumaczeniach, które wykonujesz - co potrafisz po angielsku" } 

196 
00:18:07,782 --> 00:18:15,871 
>> MICHAŁ: Well, I offer translations. Since I use and learn and practice English language for over 20 years... 
{ "Tak więc, oferuję usługi tłumaczeń. Ponieważ używam, uczę się i ćwiczę język angielski przez ponad 20 lat." } 

197 
00:18:15,897 --> 00:18:17,413 
>> MAREK: Have you ever been to USA? 
{ "Czy byłeś kiedyś w USA?" } 

198 
00:18:17,533 --> 00:18:22,722 
>> MICHAŁ: Yes, I've been in and out of the USA since I was about 8 years old. 
{ "Tak, od 8 roku życia bywałem w Stanach Zjednoczonych" } 

199 
00:18:23,796 --> 00:18:30,538 
I lived there between 1996 and 2003, continuosly for 7 years. 
{ "Mieszkałem tam pomiędzy 1996 a 2003, 7 lat pod rząd" } 

200 
00:18:30,971 --> 00:18:37,566 
I've finished my high school in the United States, I've studied a little bit... { "Ukończyłem szkolę średnią w USA, studiowałem trochę..." } 
>> MAREK: You are well educated in English. { "Znasz dobrze język angielski." }

201 
00:18:37,800 --> 00:18:51,000 
>> MICHAŁ: ...but since my father died in 1999, since '98 I was on my own and I worked in information technology, as a software developer, as many different roles. 
{ "... od 1998 roku bylem na swoim, pracowałem w informatyce, m.in. na stanowisku dewelopera oprogramowania, oraz wielu innych." } 

202 
00:18:51,200 --> 00:18:56,000 
>> MAREK: Let's switch back to Polish. { "Przejdźmy z powrotem na język angielski." } 
Wielu ludzi nie rozumie o czym mówisz. Powiedz o swoim wykształceniu informatycznym teraz z kolei. 

203 
00:18:56,100 --> 00:19:17,000 
>> MICHAŁ: Moje wykształcenie - w cudzysłowiu, ponieważ ja się uczyłem robiąc, przez doświadczenie, ponieważ nie było mnie stać aby opłacić sobie studia natomiast od '98 roku pracuję w informatyce, uczestniczyłem w ponad 30 projektach, większość z nich była pomyślnie zrealizowana. 

204 
00:19:17,100 --> 00:19:31,000 
Przez 7 lat mieszkałem, pracowałem w Stanach Zjednoczonych, gdzie skończyłem liceum, później od '98 roku pracowałem właśnie w informatyce. W 2003 roku wróciłem do Polski, także od ponad 10 lat jestem w Polsce. 

205 
00:19:31,100 --> 00:19:38,000 
Także to co ja mogę zaoferować to m.in. właśnie nauka j. angielskiego, tłumaczenia z/do j. angielskiego, korekty redaktorskie. 
>> MAREK: Mówisz informatyczne tłumaczenia? 

206 
00:19:38,100 --> 00:19:49,500 
>> MICHAŁ: No oczywiście, specjalistyczne tłumaczenia jak najbardziej - specjalistyczne i ogólne tłumaczenia. Natomiast tez oczywiście cokolwiek co ma wspólnego z informatyką, czyli tworzenie serwisów www, oprogramowania... 

207 
00:19:49,750 --> 00:20:10,000 
>> MAREK: Sklep internetowy? 
>> MICHAŁ: ... sklepy internetowe, CMSy, aplikacje bazodanowe, konsulting, doradztwo informatyczne, marketing internetowy - praktycznie cokolwiek związanego z komputerami lub internetem. Dzięki mojemu ponad 10 letniemu doświadczeniu informatycznemu, pracowałem w większości branż - ponad 30 projektów zostało zrealizowanych, tak jak już wspomniałem. 

208 
00:20:10,100 --> 00:20:21,000 
Ale od 6 lat też zajmuję się innymi tematami - oprócz języka angielskiego, oprócz informatyki, zajmuję się też medycyną energetyczną / alternatywną tzw. oraz wellness. 

209 
00:20:21,100 --> 00:20:23,000 
>> MAREK: Masz jakieś kursy pokończone też? 

210 
00:20:23,100 --> 00:20:36,000 
>> MICHAŁ: Skończyłem m.in. kurs z towaroznawstwa zielarskiego, z dyplomem oczywiście. Ponad 6 lat jestem praktykiem w medycynie energetycznej. Posiadam specjalistyczne urządzenia - jedno takie urządzenie kosztuje ponad 12 tys złotych. 

211 
00:20:36,100 --> 00:20:43,000 
Niestety, oczywiście mógłbym je sprzedać, gdyby tylko ludzie mieli troszkę większa świadomość o medycynie energetycznej. 

212 
00:20:43,100 --> 00:20:47,000 
Mam 3 takie urządzenia - dwa są w zastawie, ze względu na problemy finansowe, musiałem je zastawić. 

213 
00:20:47,100 --> 00:20:55,500 
Jedno sobie zostawiłem, no bo bez tych urządzeń, nie wiem czy bym w ogóle nadal żył tak na prawdę. Ponieważ, to co miało miejsce, pozwala mi oczyszczać na bieżąco. 

214 
00:20:55,750 --> 00:20:56,800 
>> MAREK: Zapper m.in.? 

215 
00:20:57,100 --> 00:21:03,000 
>> MICHAŁ: Zapper oczywiście ale też właśnie medycyna energetyczna. Dzięki temu, mimo ingerencji wielokrotnych w mój organizm, nadal żyję. 

216 
00:21:05,100 --> 00:21:17,000 
Będzie dołączony tak samo link do serwisu (www.vibrationalhealing.pl), który mam od kilku miesięcy postawiony. Tam będą wszystkie informacje, jeżeli ktoś jest zainteresowany usługami w tej dziedzinie, też bardzo dziękuję za wszelką pomoc. 

217 
00:21:17,100 --> 00:21:28,000 
>> MAREK: Ja apeluje do wszystkich właścicieli sklepów zielarskich - jeżeli chcecie Państwo pomóc Michałowi, zaoferujcie mu jakieś zlecenie po prostu. Nie będą jakieś wielkie opłaty za to, prawda? 

218 
00:21:28,100 --> 00:21:53,000 
>> MICHAŁ: Ja na vibrationalhealing.pl mam tez oczywiście informacje, że jest kwota minimalna, ale nie ma kwoty maksymalnej. Jeżeli ktoś chce okazać wdzięczność za moją pracę przez ostatnie tak na prawdę 6 już lat, kiedy robiłem w większości za darmo i z mojej kieszeni płaciłem - to są dziesiątki tysięcy czy setki tysięcy złotych, które zostały zainwestowane w to co robiłem - ja nie mowię tylko o akcji anty-szczepionkowej. 

219 
00:21:54,100 --> 00:22:21,000 
No i plus oczywiście to co miało miejsce jako reperkusje, przez tych którzy uważają, że mają prawo decydować, o czym można mówić, o czym nie można mówić. Więc, jeżeli ktoś może wdzięczność okazać w ten sposób, ja nie mam nic przeciwko oczywiście. Natomiast, ja nie siedzę na ulicy ani pod budką z piwem i nie proszę o pieniądze. Ja cały czas to co mogę robić to robię, więc jeżeli ktoś może wdzięczność swoją okazać, będę też wdzięczny. 

220 
00:22:22,100 --> 00:22:39,500 
>> MAREK: Proszę Państwa, jest tyle książek do przetłumaczenia na polski czy polskich na angielski. Jeżeli ktoś by chciał np. wydać swoją książkę po angielsku, możemy mu służyć pomocą. Michał jest doskonały jeżeli chodzi o tłumaczenie. No i potrzeba takich ludzi. To jest po prostu niewiarygodne, że taka osoba jak Michał w tej chwili po prostu jak żebranie to wygląda. 

221 
00:22:40,100 --> 00:23:05,000 
>> MICHAŁ: No może żebranie nie, ale jest to element tez zabezpieczenia się oczywiście. Natomiast, nie pozostało mi nic innego jak zacząć mówić o tych sprawach. Oczekuję, że w końcu będzie mnie stać na to, żeby odpowiednio te sprawy załatwić. Ponieważ, to jest nagminne łamanie praw człowieka. To jest systematyczne poniżanie mnie, blokowanie... 

222 
00:23:05,100 --> 00:23:22,000 
>> MAREK: Może wspomnę o tym jeszcze, że m.in. dzięki Tobie, ten poziom świadomości społeczeństwa od 2009 roku podskoczył niebotycznie. W tej chwili ja widzę po moim blogu, ile ludzi rozumie istotne sprawy, a przedtem kiedy zaczynaliśmy - stukali się nam w głowę. 

223 
00:23:22,100 --> 00:23:38,000 
>> MICHAŁ: Ja mam jakiś tam wkład na pewno w to, oczywiście. Bo, tak jak mówię, no mimo tego że 'świńska sprawa' - ten serwis został zawieszony w 2010 roku -  ale ja działam cały czas i działałem na wielu innych frontach. Może nie będę wchodził w niektóre tematy, ponieważ... 

224 
00:23:38,100 --> 00:23:40,000 
>> MAREK: Może w następnych rozmowach? 

225 
00:23:40,100 --> 00:23:58,000 
>> MICHAŁ: Może w następnych rozmowach. Natomiast, no tak jak mówię - dziękuję Ci Marku, że mogliśmy się spotkać i może przy następnych rozmowach będziemy mieć też okazję, żeby porozmawiać o tych tematach. Bo tak jak wspomnieliśmy - to nie są tylko szczepienia. Jest tych tematów bardzo dużo. A tak na prawdę chodzi o depopulacje na wielką skalę. 

226 
00:23:58,100 --> 00:24:16,000 
>> MAREK: Proszę Państwa, chodzi o nasze życie. O życie nas wszystkich - Waszych dzieci, Waszych wnuków. Jeżeli w ogóle dożyjemy do tego czasu, kiedy będą wnuki. Nie, myślę, że wygramy jednak Michał i myślę, że możliwość publikacji tego typu rozmów w internecie, nawet za pomocą serwisu, który jest w posiadaniu tych ludzi - 'YouTube' - tez świadczy o tym, że jesteśmy silni. 

227 
00:24:16,100 --> 00:24:45,000 
>> MICHAŁ: Przede wszystkim trzeba mówić - nie bać się mówić. I oczywiście dochodzić, przez doświadczenie i przez własną pracę, co jest prawdą, bo w naszych czasach bardzo trudno jest uchwycić to co prawdziwe, a co wirtualne. Więc, najważniejsze żeby nie dać się zastraszyć. Mimo tego wszystkiego co ma miejsce, jednak są pewne wartości, które warto jest podtrzymać. 

228 
00:24:46,100 --> 00:25:10,500 
Także, mi ust nikt nie zamknie, mimo tego wszystkiego co miało miejsce. A dzięki Markowi, mam nadzieję, że dojdziemy do większej grupy ludzi i, tak jak mowię, wystarczy mi zlecić pracę. Ja tłumaczenia robiłem, robię cały czas. Tylko, że jest ich po prostu na tyle mało, że nie jestem w stanie na razie z tego przeżyć normalnie. Natomiast, jak najbardziej książki właśnie, czy publikacje jakiekolwiek... 

229 
00:25:10,750 --> 00:25:12,500 
>> MAREK: W dwie strony? 

230 
00:25:13,100 --> 00:25:24,500 
>> MICHAŁ: ... W dwie strony. Z języka angielskiego na język polski, z języka polskiego na język angielski. To są rzeczy, które bez problemu mogę robić. Mogę tez wykonywać oczywiście lekką pracę fizyczną. Natomiast, nie mogę niestety ze względu na swoje zdrowie jeszcze normalnie iść na budowę czy jakieś takie rzeczy. 

231 
00:25:25,100 --> 00:25:40,000 
>> MAREK: Nie, nie o to chodzi. Drodzy Państwo - kontakt do Michała będzie podany poniżej, w linku, także będzie się pokazywał w trakcie tego wywiadu, po zmontowaniu. Natomiast zapraszam już teraz Państwa na kolejną rozmowę z Michałem, tym razem będzie na temat... 

232 
00:25:40,100 --> 00:25:53,000 
>> MICHAŁ: Na trochę lżejszy temat. 
>> MAREK: ... na trochę lżejszy temat. Będziemy właśnie rozmawiać o tych rzeczach, które nas trapią. Jesteś znakomitym specjalistą. Ja widzę, jaka jest wiedza Michała. Ty pogłębiłeś swoją wiedzę w trakcie tych ostatnich 4 lat. 

233 
00:25:53,100 --> 00:26:03,000 
>> MICHAŁ: Ja cały czas pogłębiam wiedzę. I tak zwany 'rabbit hole', czyli ta 'królicza nora', jest tak głęboka, że czym głębiej się wchodzi, tym bardziej sobie zdajesz (sprawę), że wiesz że nic nie wiesz. 

234 
00:26:03,100 --> 00:26:13,000 
Trzeba pamiętać, że ludzie, którzy się śmieją, mówiąc o tym, że jest jakiś spisek, czy że to są teorie spiskowe, nie maja pojęcia o czym mówią... 

235 
00:26:13,100 --> 00:26:14,200 
>> MAREK: Albo są agentami. 

236 
00:26:14,500 --> 00:26:32,000 
>> MICHAŁ: Albo są agentami, albo nie mają pojęcia o czym mówią. Mówi się, że 'ignorance is bliss'. Te publikacje są normalnie dostępne dla wszystkich - wystarczy spędzić 15 minut dziennie, żeby się doinformować. Albo po prostu nauczyć się pokory i nie mówić nic, a nie mówić bzdury. Także taka moja rada z mojej strony. Dziękuję bardzo. 

237 
00:26:32,100 --> 00:26:34,000 
>> MAREK: Dziękuję bardzo za rozmowę. 

